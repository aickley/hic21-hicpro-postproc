import numpy as np
import sys

def getmask(n):
    return (1 << n) - 1

def split(coords):
    return (
        (coords >> 32).astype(np.uint32),
        (coords & getmask(32)).astype(np.uint32),
    )

def getchr(coord):
    return coord >> 23

def getbin(coord):
    return (coord >> 3) & getmask(20)


l = np.load(sys.argv[1])

try:
    count = int(sys.argv[2])
except:
    count = 50

if count > 0:
    start, stop = None, count
else:
    start, stop = count, None


cpairs = l['index'][start: stop]
counts = l['counts'][start: stop]

print(cpairs.shape)
print(counts.shape)

coords = split(cpairs)

print(np.vstack([
    getchr(coords[0]),
    getbin(coords[0]),
    getchr(coords[1]),
    getbin(coords[1]),
    counts
]).T)
