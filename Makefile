CXXFLAGS=-O3 -Wall
INC=-I include -I cnpy -I deps/optparse

all : bin/parse bin/print bin/getas bin/count bin/test_fd

cnpy/libcnpy.a : cnpy/cnpy.h cnpy/cnpy.cpp
	cd cnpy && cmake . && make cnpy-static

bin/% : src/%.cpp include/*.hpp cnpy/libcnpy.a
	mkdir -p bin
	g++ ${CXXFLAGS} ${INC} $< cnpy/libcnpy.a -lz -std=c++14 -o $@

clean :
	rm -rf hicpro2numpy
	cd cnpy && git clean -d -f -x
