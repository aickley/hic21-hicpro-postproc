## Bugs

# Duplicate Index Entries
Duplicate index entry in TwT3:
- value: 864693121529675776
- first coordinate (chr,bin): 24,58
- second coordinate (chr,bin): 58,0
Noted on 14.09.2018
