#include <type_traits>


namespace {

// utility

template<typename Packed,
         uint8_t offset,
         uint8_t width,
         typename=typename std::enable_if<
            (offset + width <= 8 * sizeof(Packed)), void>::type>
struct mask {
    static constexpr Packed get() {
        return (
            mask<Packed, offset, 1>::get()
            | mask<Packed, offset+1, width-1>::get());
    }
};


template<typename Packed,
         uint8_t offset>
struct mask<Packed, offset, 1> {
    static constexpr Packed get() {
        return Packed(1) << offset;
    }
};

}


template<typename Packed,
         uint8_t offset,
         uint8_t width,
         typename Unpacked=Packed>
struct packed_field {

    static Packed pack(Unpacked value) {
        Packed v = value;
        v <<= offset;
        v &= mask<Packed, offset, width>::get();
        return v;
    }
 
    static Unpacked get(Packed packed) {
        packed &= mask<Packed, offset, width>::get();
        packed >>= offset;
        return static_cast<Unpacked>(packed);
    }
};
