#pragma once

#include <stdint.h>
#include <array>
#include <string>

#include "packed_field.hpp"


using ChrId = unsigned char;


namespace coord {
    using type = uint32_t;

    using Rev = packed_field<type, 0, 1, bool>;
    using Allele = packed_field<type, 1, 2, uint8_t>;
    using Bin = packed_field<type, 3, 20>; // can be resfrag or bin
    using Chr = packed_field<type, 23, 6, ChrId>;

    inline auto rev(type d) noexcept { return Rev::get(d); }
    inline auto allele(type d) noexcept { return Allele::get(d); }
    inline auto bin(type d) noexcept { return Bin::get(d); }
    inline auto chr(type d) noexcept { return Chr::get(d); }

    inline type reset_flags(type d) noexcept { return (d >> 3) << 3; }

    // for comparison
    static inline type key(type data) noexcept { return data >> 3; }
    static inline bool samebins(type a, type b) noexcept {
        return ~((a ^ b) >> 3);
    }
};

/// Packed pair of coordinates
namespace cpair {
    using type = uint64_t;
    //using item_type = typename coord::type;

    using Second = packed_field<type, 0, 32, coord::type>;
    using First = packed_field<type, 32, 32, coord::type>;
    
    inline auto first(type d) noexcept { return First::get(d); }
    inline auto second(type d) noexcept { return Second::get(d); }
    static constexpr type na = 0;
    inline type is_na(type d) noexcept { return d == na; }

    inline type reset_flags(type d) {
        return (
            First::pack(coord::reset_flags(first(d)))
            | Second::pack(coord::reset_flags(second(d)))
        );
    }

    inline auto swap(type d) noexcept {
        return First::pack(second(d)) | Second::pack(first(d));
    }
};

/// Packed pair of alleles
namespace apair {
    using type = uint8_t;

    using Second = packed_field<type, 0, 2, coord::type>;
    using First = packed_field<type, 2, 2, coord::type>;

    inline type apair(cpair::type d) noexcept {
        return (First::pack(coord::allele(cpair::first(d)))
                | Second::pack(coord::allele(cpair::second(d))));
    };

    inline auto first(type d) noexcept { return First::get(d); }
    inline auto second(type d) noexcept { return Second::get(d); }
    inline bool canonical(type d) noexcept { return first(d) < second(d); }

    std::string to_string(type d) noexcept {
        std::string s(" - ");
        s[0] = first(d) + '0';
        s[2] = second(d) + '0';
        return s;
    };
};

