#pragma once

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>

#include <exception>
#include <string>

#include "strutil.hpp"

namespace {

std::string fread(size_t size, FILE *fp) {
    std::string result;
    result.resize(size);
    fread(&result[0], sizeof(char), result.size(), fp);
    return result;
}

}

namespace ik {
namespace fd {

// Auxilary

/// Get path of a descriptor
std::string get_path(int fd) {
    static constexpr size_t cmdcap = 1 << 10; 
    static constexpr size_t pathcap = 1 << 10; 
    auto cmd = ik::str::fmt(cmdcap, "readlink /proc/self/fd/%d", fd);
    auto out = popen(cmd.c_str(), "r");
    auto path = fread(pathcap, out);
    bool ok = ik::str::trim(path, '\n');
    return ok ? path : std::string("<path unavailable>");
}

// Error message generators

namespace {

using namespace ik::str;
static constexpr size_t dc = 1 << 10;

std::string open_error(const char *path) {
    auto res = fmt(dc, "Could not open file %s", path);
    return res;
}

std::string read_write_error(int fd, const char *op,
                             const char *reason=nullptr)
{
    auto path = get_path(fd);
    if (!reason)
        return fmt(dc, "Error occured while %s from file %s (fd %d)",
                   op, path.c_str(), fd);
    else 
        return fmt(dc, "Error occured while %s from file %s (fd %d): %s",
                   op, path.c_str(), fd, reason);
}

}

// Exception classes

struct FdError : public std::runtime_error {
  public:
    FdError(std::string what)
        : std::runtime_error(what.c_str()) // force deep copy
    {}
};

struct OpenError : public FdError {
    OpenError(const char *path) : FdError(open_error(path)) {}
};

class ReadWriteError : public FdError {
    int fd_;
  public:
    ReadWriteError(int fd, const char *op, const char *reason=nullptr)
        : FdError(read_write_error(fd, op, reason))
        , fd_(fd)
    {}
    int fd() const noexcept { return fd_; }
};

class EndOfFile : public ReadWriteError {
  public:
    EndOfFile(int fd)
        : ReadWriteError(fd, "reading", "end of file")
    {}
};


// Actual syscall wrappers

int open(const char* path, int flags) {
    ssize_t fd = ::open(path, flags);
    if (fd < 0)
        throw OpenError(path);
    else
        return fd;
}


int open(const char* path, int flags, mode_t mode) {
    ssize_t fd = ::open(path, flags, mode);
    if (fd < 0)
        throw OpenError(path);
    else
        return fd;
}

size_t read(int fd, void *data, size_t capacity, bool eof_throws=false) {
    ssize_t ret = ::read(fd, data, capacity);
    if (ret < 0) {
        throw ReadWriteError(fd, "reading");
    } else if (ret == 0 && eof_throws){
        throw EndOfFile(fd);
    } else {
        return static_cast<size_t>(ret);
    }
}

template<typename T>
size_t tread(int fd, T* begin, T* end, bool eof_throws=false) {
    size_t capacity = (end - begin) * sizeof(T);
    return read(fd, begin, capacity, eof_throws) / sizeof(T);
}

size_t write(int fd, void *data, size_t size_bytes) {
    ssize_t ret = ::write(fd, data, size_bytes);
    if (ret < 0) {
        throw ReadWriteError(fd, "writing");
    } else {
        return static_cast<size_t>(ret);
    }
}

template<typename T>
size_t twrite(int fd, T* begin, T* end) {
    size_t size = (end - begin) * sizeof(T);
    return write(fd, begin, size) / sizeof(T);
}

}
}
