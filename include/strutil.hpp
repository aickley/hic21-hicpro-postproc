#pragma once

#include <stdio.h>
#include <stdarg.h>

#include <string>
#include <stdexcept>


namespace ik {
namespace str {

struct FormatException : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

struct CapacityException : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

/// Returns a string with a given capacity
std::string reserve(size_t size) {
    std::string result;
    result.reserve(size);
    return result;
}

/// Trim string to the first occurence of a character (before or after)
bool trim(std::string& str, char termchr='\0', bool after=false) {
    auto pos = str.find_first_of(termchr);
    auto ok = pos != std::string::npos;
    if (ok) str.resize(pos + after);
    return ok;
}

/// Returns the difference between capacity and required capacity
int vstrprintf(std::string& out, const char *format, va_list args) {
    int required = ::vsnprintf(&out[0], out.capacity(), format, args);
    if (required < 0)
        throw FormatException(format);
    return out.capacity() - required;
}

/// Returns the difference between capacity and required capacity
int strprintf(std::string& out, const char *format, ...) {
    va_list args;
    va_start(args, format);
    return vstrprintf(out, format, args);
}

/// Raises if message cannot be formated within capacity
std::string strprintf(size_t capacity, const char *format, ...) {
    va_list args;
    va_start(args, format);
    auto out = reserve(capacity);
    if (vstrprintf(out, format, args) < 0)
        throw CapacityException(format);
    return out;
}

/// Reallocates if extra capacity is needed (at the cost of always copying args)
std::string fmt(size_t capacity, const char *format, ...) {
    va_list args, backup;
    va_start(args, format);
    va_copy(backup, args);
    auto out = reserve(capacity);
    auto n = vstrprintf(out, format, args);
    if (n < 0) { // extra capacity needed
        out.reserve(capacity - n);
        vstrprintf(out, format, backup);
    }
    return out;
}

}
}
