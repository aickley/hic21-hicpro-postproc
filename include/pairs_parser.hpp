#pragma once

#include <string>

#include "fd.hpp"
#include "coords.hpp"
#include "eprintf.hpp"


struct ParsingError : std::exception {
    static constexpr size_t msgCapacity = 1 << 10;
    std::string msg_;

  public:

    ParsingError(const char* reason, size_t lineno, size_t posInLine,
                 const char* bufcur, const char* buflast)
    {
        auto msgcur = &msg_[0];
        auto msglast = msgcur + msgCapacity - 1;
        msgcur += snprintf(msgcur, msgCapacity,
                           "Error while parsing line %zd col %zd: %s. read buffer:\n",
                           lineno, posInLine, reason);
        while (msgcur < msglast && bufcur < buflast && *bufcur != '\n')
            *msgcur++ = *bufcur++;
        *msgcur = '\0';
    }
    
    const char* what() const noexcept { return &msg_[0]; }
};


class PairsParser {
    int fd_;
    std::string buf_;
    const char *cur_ = nullptr;
    const char *last_ = nullptr;

    size_t lineno = 1;
    size_t posInLine = 0;
    size_t bytes_read = 0;
    size_t gb_read = 0;

    inline bool atEnd() noexcept { return cur_ == last_; }

    inline char nextChar() {
        if (atEnd()) {
            // EndOfFile exception thrown if no data is read
            auto size = ik::fd::read(fd_, &buf_[0], buf_.capacity(), true);
            cur_ = buf_.data();
            last_ = cur_ + size;
            bytes_read += size;
            if (gb_read != (bytes_read >> 30)) {
                gb_read = (bytes_read >> 30);
                eprintf("%3zd GB read\n", gb_read );
            }
        }
        ++posInLine;
        return *cur_++;
    }

    inline void eatChar(char c) {
        if (nextChar() != c)
            throw ParsingError("unexpected character", lineno, posInLine, cur_, last_);
    }

    inline void eatString(const char *s) {
        while (*s != 0 && nextChar() == *s)
            ++s;
    }

    template<typename T=unsigned char>
    inline T strHash(char terminator) {
        T hash{};
        while (nextChar() != terminator)
            hash = hash * 31 + *cur_;
        return hash;
    }

    inline void skipUntil(char c, size_t count=1) {
        size_t found = 0;
        while (found < count)
            found += (nextChar() == c);
    }

    inline bool parseDigit(char c, char& out) {
        int x = c - '0';
        if (x < 0 || x > 10) return false;
        out = static_cast<char>(x);
        return true;
    }

    template<typename T>
    inline T parseDecimal(T already_read=0) {
        T res{already_read};
        unsigned char n_digits{};
        char d;
        while (parseDigit(nextChar(), d)) {
            res *= 10;
            res += d;
            ++n_digits;
        }
        if (!n_digits && !already_read) {
            throw ParsingError("unexpected digit or zero digits", lineno, posInLine, cur_, last_);
        }
        return res;
    }

    inline ChrId parseChr() {
        eatString("chr");
        char c = nextChar();
        char out{};
        if (parseDigit(c, out))
            return parseDecimal(out);
        // non-numeric chromosome
        switch (c) {
            case 'X': out = 23; break;
            case 'Y': out = 24; break;
            case 'M': out = 25; break;
        }
        // skip next field separator
        eatChar('\t');
        return out;
    }

  public:
    PairsParser(int fd, size_t capacity)
        : fd_(fd)
    {
        buf_.reserve(capacity);
        //readahead(fd_.fd, capacity, 1 << 12);
    }
    
    typename cpair::type readPair(int binSize) {
        typename coord::type c[2] = {0, 0};
        using namespace coord;
        using namespace cpair;

        try {
            skipUntil('\t', 1);
            c[0] |= Chr::pack(parseChr());
            if (binSize)
                c[0] |= Bin::pack(parseDecimal<uint32_t>() / binSize);
            else
                skipUntil('\t', 1);
            c[0] |= Rev::pack(nextChar() == '+' ? 0 : 1);
            
            eatChar('\t');
            c[1] |= Chr::pack(parseChr());
            if (binSize)
                c[1] |= Bin::pack(parseDecimal<uint32_t>() / binSize);
            else
                skipUntil('\t', 1);
            c[1] |= Rev::pack(nextChar() == '+' ? 0 : 1);

            skipUntil('_', 2);
            if (!binSize)
                c[0] |= Bin::pack(parseDecimal<uint32_t>());
            else
                skipUntil('\t', 1);
            skipUntil('_', 2);
            if (!binSize)
                c[1] |= Bin::pack(parseDecimal<uint32_t>());
            else
                skipUntil('\t', 1);

            skipUntil('\t', 2);
            c[0] |= Allele::pack(parseDecimal<uint8_t>());
            c[1] |= Allele::pack(parseDecimal<uint8_t>());

            return First::pack(c[0]) | Second::pack(c[1]);
        } catch (ik::fd::EndOfFile) {
            return cpair::na;
        }

        ++lineno;
        posInLine = 0;
    }
};
