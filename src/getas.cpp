#include <string>
#include <stdint.h>
#include <vector>
#include <map>

#include <cnpy.h>

#include "eprintf.hpp"
#include "coords.hpp"
#include "fd.hpp"


void print_pair(cpair::type p) {
    auto c0 = cpair::first(p);
    auto c1 = cpair::second(p);
    using namespace coord;
    printf("%3d %8d\t%3d %8d\t%c %c\t%c%c\n",
           chr(c0), bin(c0),
           chr(c1), bin(c1),
           "+-"[rev(c0)], "+-"[rev(c1)],
           ".12"[allele(c0)], ".12"[allele(c1)]);
}

cpair::type normalize(cpair::type d) {
    if (!apair::canonical(apair::apair(d)))
        return cpair::swap(d);
    else
        return d;
}


int main(int argc, const char* argv[]) {

    if (argc != 2) {
        eprintf("Usage: %s [INPUT FILE]\n\n", argv[0]);
        eprintf("Writes allele specific pairs to [INPUT FILE].as.npz\n\n");
        exit(-1);
    }

    std::string inPath(argv[1]);
    std::string outPath = inPath + ".as.npz";
    
    int fdin = ik::fd::open(inPath.data(), O_RDONLY);
    
    eprintf("Reading from '%s'...\n", inPath.data());
    static constexpr size_t BufSize = 1 << 20;
    std::vector<typename cpair::type> buf;
    std::map<apair::type, std::vector<cpair::type>> aspecific;
    buf.resize(BufSize);

    size_t i = 0;
    while (auto n = ik::fd::read(fdin, &buf[0], BufSize)) {
        buf.resize(n);
        for (auto p: buf) {
            p = normalize(p);
            auto alleles = apair::apair(p);
            if (alleles) {
                aspecific[alleles].push_back(p);
            }
            ++i;
        }
    }

    static const char writeMode[] = "w";
    static const char appendMode[] = "a";

    eprintf("Writing allele specific pairs to '%s'...\n", outPath.data());
    bool first = true;
    for (auto x: aspecific) {
        auto label = apair::to_string(x.first);
        cnpy::npz_save(outPath.data(), label.data(), &(x.second[0]), {x.second.size()},
                       first ? writeMode : appendMode);
        eprintf("%s: %zd items written\n", &label[0], x.second.size());
        first = false;
    }
}
