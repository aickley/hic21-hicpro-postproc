#include <stdio.h>
#include <stdlib.h>

#include <string>
#include <algorithm>
#include <vector>

#include <optparse.h>
#include <cnpy.h>

#include "fd.hpp"
#include "pairs_parser.hpp"
#include "eprintf.hpp"


struct Settings {
    int fdin = 0;
    int fdout = 1;
    size_t binsize = 0;
    size_t inbufsize = 1 << 20;
    size_t outbufsize = 1 << 20;
};

void print_pair(cpair::type p) {
    auto c0 = cpair::first(p);
    auto c1 = cpair::second(p);
    using namespace coord;
    eprintf("%3d %8d\t%3d %8d\t%c %c\t%c%c\n",
           chr(c0), bin(c0),
           chr(c1), bin(c1),
           "+-"[rev(c0)], "+-"[rev(c1)],
           ".12"[allele(c0)], ".12"[allele(c1)]);
}

Settings parse_argv(int argc, char* argv[]) {
    optparse::OptionParser optp
        = optparse::OptionParser()
            .description("Convert HiC-Pro validPairs file to binary format");

    optp.add_option("-i", "--in")
        .dest("in")
        .help("read input from PATH (stdin if omitted)")
        .metavar("PATH");

    optp.add_option("-o", "--out")
        .dest("out")
        .help("write output to PATH (stdout if omitted)")
        .metavar("PATH");

    optp.add_option("-b", "--bin")
        .dest("binsize")
        .help("size of bins to use (resfrags if omitted)")
        .set_default("0")
        .metavar("SIZE");

    const optparse::Values options = optp.parse_args(argc, argv);
    const std::vector<std::string> args = optp.args();
    Settings settings;

    if (options.is_set("in"))
        settings.fdin = ik::fd::open(options["in"].c_str(), O_RDONLY);
    if (options.is_set("out")) {
        settings.fdout = ik::fd::open(options["out"].c_str(),
                                      O_WRONLY|O_CREAT|O_TRUNC, 0644);
        //printf("out = %s %d\n", &options["out"][0], settings.fdout);
        //printf("out = %zd\n", settings.fdout);
    }
    settings.binsize = atoi(options["binsize"].c_str());


    return settings;
}

bool cmp(cpair::type& a, cpair::type& b) {
    return coord::key(cpair::second(a)) < coord::key(cpair::second(b));
}

void mainloop(const Settings& s) {
    PairsParser parser{s.fdin, s.inbufsize};
    typename cpair::type pair;
    std::vector<typename cpair::type> group{};
    std::vector<typename cpair::type> outbuf{};

    auto get_key = [](cpair::type pair){
        return coord::key(cpair::first(pair)); };

    do {
        pair = parser.readPair(s.binsize);
        auto group_key = get_key(pair);
        auto cur_key = group_key;
        do {
            group.push_back(pair);
            pair = parser.readPair(s.binsize);
            cur_key = get_key(pair);
        } while (!cpair::is_na(pair) && group_key == cur_key);

        std::sort(group.begin(), group.end(), cmp);
        for (auto item: group)
            outbuf.push_back(item);

        group.clear();
        group.push_back(pair);

        if (outbuf.size() > s.outbufsize) {
            ik::fd::twrite(s.fdout, &outbuf[0], &outbuf[outbuf.size()]);
            outbuf.clear();
        }
    } while (!cpair::is_na(pair));

    if (outbuf.size()) {
        ik::fd::twrite(s.fdout, &outbuf[0], &outbuf[outbuf.size()]);
    }

    close(s.fdin);
    close(s.fdout);
}

int main(int argc, char* argv[]) {
    auto settings = parse_argv(argc, argv);
    mainloop(settings);
}
