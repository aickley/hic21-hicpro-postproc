#include <string>
#include <stdint.h>
#include <vector>

#include <optparse.h>

#include "coords.hpp"
#include "fd.hpp"


void print_pair(cpair::type p) {
    auto c0 = cpair::first(p);
    auto c1 = cpair::second(p);
    using namespace coord;
    printf("%3d %8d\t%3d %8d\t%c %c\t%c%c\n",
           chr(c0), bin(c0),
           chr(c1), bin(c1),
           "+-"[rev(c0)], "+-"[rev(c1)],
           ".12"[allele(c0)], ".12"[allele(c1)]);
}

struct Settings {
    int fdin = 0;
    //int fdout = 1;
    size_t inbufsize = 1 << 10;
};

Settings parse_argv(int argc, const char* argv[]) {
    optparse::OptionParser optp
        = optparse::OptionParser()
            .description("Print bin pairs");

    optp.add_option("-i", "--in")
        .dest("input")
        .help("read input from PATH (stdin if omitted)")
        .metavar("PATH");

    //optp.add_option("-o", "--out")
    //    .dest("output")
    //    .help("write output to PATH (stdout if omitted)")
    //    .metavar("PATH");


    const optparse::Values options = optp.parse_args(argc, argv);
    //const std::vector<std::string> args = optp.args();
    Settings settings;

    if (options.is_set("in"))
        settings.fdin = ik::fd::open(options["in"].c_str(), O_RDONLY);
    //if (options.get("out"))
    //    settings.fdout = ik::fd::open(options["out"].c_str(),
    //                                  O_WRONLY|O_CREAT|O_TRUNC, 644);   
    return settings;
}

void mainloop(const Settings& s) {
    std::vector<typename cpair::type> buf;
    buf.resize(s.inbufsize);

    while (auto n = ik::fd::tread(s.fdin, &buf[0], &buf[buf.size()])) {
        buf.resize(n);
        for (auto p: buf)
            print_pair(p);
    }
}

int main(int argc, const char* argv[]) {
    auto settings = parse_argv(argc, argv);
    mainloop(settings);
}
