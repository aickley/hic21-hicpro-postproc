#include <stdint.h>
#include <stdio.h>
#include "packed.hpp"


int main() {
    uint32_t packed = 0;
    using f1 = packed_field<uint32_t, 0, 3>;
    using f2 = packed_field<uint32_t, 3, 3>;
    packed |= f1::pack(3) | f2::pack(4);
    //packed = pack<3, 6>(packed, 2);
    printf("%d %d\n", f1::unpack(packed), f2::unpack(packed));
    return 0;
}
