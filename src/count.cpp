#include <string>
#include <stdint.h>
#include <vector>

#include <cnpy.h>

#include "coords.hpp"
#include "fd.hpp"

void print_pair(cpair::type p) {
    auto c0 = cpair::first(p);
    auto c1 = cpair::second(p);
    using namespace coord;
    printf("%3d %8d\t%3d %8d\t%c %c\t%c%c\n",
           chr(c0), bin(c0),
           chr(c1), bin(c1),
           "+-"[rev(c0)], "+-"[rev(c1)],
           ".12"[allele(c0)], ".12"[allele(c1)]);
}

/// Counts consecutive occurrences for sorted inputs
template<typename IndexValue, typename CountValue>
struct Counter {
    std::vector<IndexValue> index{};
    std::vector<CountValue> counts{};

    IndexValue curind{};
    CountValue curcnt{};

    Counter(){};

    inline void set_current(IndexValue ind) {
        if (curcnt) {
            index.push_back(curind);
            counts.push_back(curcnt);
        }
        curind = ind;
        curcnt = 0;
    }

    inline void push(IndexValue ind) {
        bool new_ = ind != curind;
        if (!curcnt || new_)
            set_current(ind);
        ++curcnt;
        //printf("%d ", curcnt);
        //print_pair(curind);
    }

    void save_npz(const char *path) {
        cnpy::npz_save(path, "index", &index[0], {index.size()}, "w");
        cnpy::npz_save(path, "counts", &counts[0], {counts.size()}, "a");
    }

};

int main(int argc, const char* argv[]) {
    int fdin = 0;

    if (argc != 2) {
        exit(-1);
    }

    Counter<cpair::type, uint32_t> c;

    static constexpr size_t BufSize = 1 << 10;
    std::vector<typename cpair::type> buf;
    buf.resize(BufSize);

    while (auto n = ik::fd::tread(fdin, &buf[0], &buf[buf.size()])) {
        for (auto i=0; i<n; ++i) {
            c.push(cpair::reset_flags(buf[i]));
        }
    }

    c.save_npz(argv[1]);
}
